// Misty Au
// Operating Systems, Spring 2019

/**
 * Simple shell interface program.
 *
 * Operating System Concepts - Ninth Edition
 * Copyright John Wiley & Sons - 2013
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <sys/wait.h>

#define MAX_LINE		80 /* 80 chars per line, per command */

int main(void)
{
	char *args[MAX_LINE/2 + 1];	/* command line (of 80) has max of 40 arguments */
	char **history = NULL;
	char **ptr = NULL;
	char line[MAX_LINE];
    int should_run = 1;
	pid_t pid;
	
	int i, upper = MAX_LINE / 2, commNum;
	static int num = 0;
	
    while (should_run){   
        printf("osh>");
        fflush(stdout);
		
		// Get user input
		fgets(line, MAX_LINE, stdin);

		if (strcmp(line, "\n") == 0)
		{
			printf("Bad input.\n");
			should_run = 0;
			continue;
		}
		
		// If exit was entered, exit the while loop
		if (strcmp(line, "exit\n") == 0)
		{
			should_run = 0;
			continue;
		}
		
		// If history was entered, display the 10 most recent commands
		if (strcmp(line, "history\n") == 0)
		{
			if (num == 0)
			{
				printf("No commands in history.\n");
			}
			else
			{
				for (i = num - 1; num <= 10 ? i >= 0 : i >= num - 10; i--)
					printf("%d %s\n", i + 1, history[i]);
			}
			continue;
		}
		
		// Execute the most recent command in history
		if (strcmp(line, "!!\n") == 0)
		{
			// todo
			if (history == NULL || num == 0)
			{
				printf("No commands in history.\n");
				continue;
			}
			
			// echo command
			printf("%s\n", history[num - 1]);
			strcpy(line, history[num - 1]);
		}
		
		
		// Execute Nth command in history
		if (line[0] == '!')
		{
			if (history == NULL || num == 0)
			{
				printf("No commands in history.\n");
				continue;
			}
			
			commNum = atoi(line + 1);
			
			if (commNum > num || commNum < 1)
			{
				printf("No such command in history.\n");
				continue;
			}
			
			printf("%s\n", history[commNum - 1]);
			strcpy(line, history[commNum - 1]);
		}
		
		// Allocate memory for history array
		ptr = realloc(history, (num + 1) * sizeof(char *));
		if (ptr == NULL)
		{
			printf("Failed allocating memory.\n");
			should_run = 0;
			continue;
		}
		history = ptr;
		
		history[num] = malloc(sizeof(char) * (strlen(line) + 1));
		if (history[num] == 0)
		{
			printf("Failed allocating memory.\n");
			should_run = 0;
			continue;
		}
		
		strcpy(history[num], line);
		num++;
		
		// Tokenize the input and store into args array
		args[0] = strtok(line, " \n");
		
		for (i = 0; args[i] != NULL && i < upper; i++)
		{
			args[i + 1] = strtok(NULL, " \n");
		}
		
		args[i] = NULL;
		
        /**
         * After reading user input, the steps are:
         * (1) fork a child process
         * (2) the child process will invoke execvp()
         * (3) if command included &, parent will invoke wait()
         */
		 
		 // Forking a child process
		 pid = fork();
		 
		 if (pid < 0) // dud
		 {
			 fprintf(stderr, "Fork failed.");
			 exit(-1);
		 }
		 else if (pid > 0) // parent
		 {
			 if (strcmp(args[i - 1], "&") != 0)
			 {
				 if (wait(NULL) == -1)
				 {
					 fprintf(stderr, "Wait failed.\n");
					 should_run = 0;
					 continue;
				 }
			 }
		 }
		 else //child
		 {
			 if (execvp(args[0], args) == -1)
			 {
				 fprintf(stderr, "Failed to execute command.\n");
				 exit(-1);
			 }
		 }
    } // End while loop

	// Deallocate memory for history array
	for (i = 0; i < num; i++)
		free(history[i]);
	
	if (history != NULL)
		free(history);
    
	return 0;
}
