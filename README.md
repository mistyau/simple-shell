# Simple Shell

A simple shell interface program written in C. Modified from Operating System Concepts - Ninth Edition, Copyright John Wiley & Sons - 2013.

This shell program allows the parent and child processes to run concurrently.
This is invoked when the user enters the following command:

`osh> cat prog.c &`

This shell program also includes a history feature. When the user enters the following command:

`osh> history`

the terminal displays back the ten most recent commands.

Additionally,

`osh> !!`

executes the most recent command in history and

`osh> !N`

where N is an integer, executes the Nth command in history.

# Running simple-shell.c:

1. Enter `gcc simple-shell.c` into the command line to compile.
2. Then enter `./a.out` to execute.
3. After successful execution, you will be greeted with the prompt `osh>` on your terminal. Enter your command after this prompt.
4. Enter `exit` to exit program.